package org.evalp.puzzle;

import static org.evalp.puzzle.Puzzle.HEX_LETTERS;
import static org.evalp.puzzle.Puzzle.LETTERS;

/**
 * @author Evgeny Pavlovsky
 */
class Converter {

  long hashFromString(String string) {
    return hashFromString(string, LETTERS);
  }

  long hashFromNumber(long number) {
    final String hexString = Long.toHexString(number);
    return hashFromString(hexString, HEX_LETTERS);
  }

  String stringFromNumber(long number) {
    final String hexString = Long.toHexString(number);
    final StringBuilder result = new StringBuilder();

    for (int i = 0; i < hexString.length(); i++) {
      int letterIndex = HEX_LETTERS.indexOf(hexString.charAt(i));
      result.append(LETTERS.charAt(letterIndex));
    }

    return result.toString();
  }

  private long hashFromString(String string, String letters) {
    long h = 7;
    for (int i = 0; i < string.length(); i++) {
      h = (h * 37 + letters.indexOf(string.charAt(i)));
    }
    return h;
  }
}
